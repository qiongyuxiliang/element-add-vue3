/**
 * @description 跨页选择
 * @param {Object} options
 * @param {String} options.key 行数据唯一标识
 * @param {Function} options.toggleRowSelection 设置行数据选中/取消选中的方法,必传
 */
type Options = {
  key: string | number
  toggleRowSelection: () => void
  data: any[]
  max?: number
}
export const CrossPage = class {
  #crossPageMap = new Map()
  key: string | number
  data: any[]
  max: number
  toggleRowSelection: any
  constructor(options: Options) {
    this.key = options.key || 'id'
    this.data = options.data || []
    this.max = options.max || Number.MAX_SAFE_INTEGER
    this.toggleRowSelection = options.toggleRowSelection as any
    if (typeof this.toggleRowSelection !== 'function')
      throw new Error('toggleRowSelection is not function')
  }
  get keys() {
    return Array.from(this.#crossPageMap.keys())
  }
  get values() {
    return Array.from(this.#crossPageMap.values())
  }
  get size() {
    return this.#crossPageMap.size
  }
  clear() {
    this.#crossPageMap.clear()
    this.updateViews()
  }
  isChecked(row: Record<any, any>) {
    return this.#crossPageMap.has(row[this.key])
  }
  onRowSelectChange(row: Record<any, any>) {
    if (typeof row !== 'object') return console.error('row is not object')
    const { key, toggleRowSelection } = this
    if (this.isChecked(row)) this.#crossPageMap.delete(row[key])
    else {
      this.#crossPageMap.set(row[key], row)
      if (this.size > this.max) {
        this.#crossPageMap.delete(row[key])
        toggleRowSelection(row, false)
      }
    }
  }
  onDataChange(list: any) {
    this.data = list
    this.updateViews()

    // const timeout = 10
    // setTimeout(() => {
    // }, timeout)
  }
  updateViews() {
    const { data, toggleRowSelection, _key } = this
    data.forEach((row: any) => {
      toggleRowSelection(row, this.isChecked(row))
    })
  }
}
