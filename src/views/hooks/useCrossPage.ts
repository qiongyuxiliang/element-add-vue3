import { CrossPage } from './CrossPage'
export type OptionsType = {
  key: string
  data: any[]
  toggleRowSelection: () => void
}
let target: any

export const useCrossPage = (options: OptionsType) => {
  target = new CrossPage({
    key: options.key || 'id',
    data: options.data,
    toggleRowSelection: options.toggleRowSelection
  })
  return target
}
