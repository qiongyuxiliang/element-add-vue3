import { ref, onMounted, nextTick } from 'vue'
import { useCrossPage } from './useCrossPage'
const currentPage = ref(1)
const pageSize = ref(10)
const tableData = ref([])
let crossPageIns: any

const getKeys = () => {
  console.log('__ZZC__🍟 ~ keys ~ crossPageIns.keys:', crossPageIns.keys)
}
const clear = () => {
  console.log(crossPageIns)
  crossPageIns.clear()
}
const getSelectRows = () => {
  console.log('__ZZC__🍟 ~ values ~ crossPageIns.values:', crossPageIns.values)
}
const currentChange = async (page: any) => {
  currentPage.value = page
  //需等待渲染完成之后再去更新table的选中状态
  await nextTick()
  crossPageIns.onDataChange(tableData.value)
}
const handleSelectChange = (_val: any, row: any) => {
  crossPageIns.onRowSelectChange(row)
}
const handleSelectAllChange = (val: any) => {
  console.log('__ZZC__🍟 ~ handleSelectAllChange ~ val:', val)
  tableData.value.forEach((row) => {
    if (val.length === 0) {
      // 取消全选
      if (crossPageIns.isChecked(row)) {
        crossPageIns.onRowSelectChange(row)
      }
    } else {
      // 全选
      if (!crossPageIns.isChecked(row)) {
        crossPageIns.onRowSelectChange(row)
      }
    }
  })
}

export const useTablePagi = ({
  id,
  tableData: tb,
  toggleRowSelection
}: {
  id: string
  tableData: any
  toggleRowSelection: any
}) => {
  tableData.value = tb.value
  onMounted(() => {
    crossPageIns = useCrossPage({
      key: id,
      data: tableData.value as any,
      toggleRowSelection: toggleRowSelection.value
    })
  })

  //异步暴露出去的方法和属性
  return {
    toggleRowSelection,
    getKeys,
    clear,
    getSelectRows,
    currentChange,
    handleSelectChange,
    handleSelectAllChange
  }
}
//同步暴露的属性
export const usePage = () => {
  return {
    currentPage,
    pageSize,
    tableData
  }
}
